# fediverse-importer
*Disclaimer: This is a very young project in the beginning stages. No code exists as of yet.

## What problem is this trying to solve?
A current sticking point for people migrating to a new social network for an old one is "What about all of my old stuff?"
It's common to completely start over from scratch, but this ignores the fact that there may be some old gems
on the previous network worth digging up and sharing.

The goal of this project is to explore the possibility of importing data archives from other social networks, and convert
the contents into ActivityPub statuses using a simple C2S client. Initially, all this will do is convert data from one schema to another, and then post it to a user account.

## How can I help?
Right now, this project is purely in the exploration phase. No backend exists yet. The best way to contribute right now
is to provide ideas and suggestions in the [issue tracker](https://codeberg.org/postmodern/fediverse-importer/issues).